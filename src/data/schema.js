import {
  makeExecutableSchema
} from 'graphql-tools';
import {
  resolvers
} from './resolvers';

const typeDefs = `

type Processo {
  id: ID!,
  tipo: String!,
  descricao: String!,
  partes: [Parte]
}

type Parte {
  id: ID!,
  nome: String!,
  processos: [Processo]
}

# Root Query - all the queries supported by the schema
type Query {

  processos: [Processo],
  processo(id: ID!): Processo,
  partes: [Parte],
  parte(id: ID!): Parte
}

# Root Mutation - all the mutations supported by the schema
type Mutation {
  updateProcesso(id: ID!, descricao: String!): Processo
  
}

# Root Subscription - all the subscriptions supported by the schema
type Subscription {
  processoUpdated: Processo
}

# schema consists of the Root Query, the Root Mutation and the Root Subscription
schema {
  query: Query,
  mutation: Mutation,
  subscription: Subscription
}
`;

// Create the schema
export const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});
