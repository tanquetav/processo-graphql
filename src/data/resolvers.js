import axios from 'axios';
import {
    PubSub
} from 'graphql-subscriptions';
import {
    config
} from '../config';

// ------------------------------------------------------------------------------------------------
// Create a PubSub instance
// ------------------------------------------------------------------------------------------------
export const pubsub = new PubSub();

// ------------------------------------------------------------------------------------------------
// Resolvers
// ------------------------------------------------------------------------------------------------
export const resolvers = {
    Query: {
        processos() {
            return axios
                .get(config.dbApi.processos)
                .then(response => response.data);
        },

        processo(root, args) {
            return axios
                .get(`${config.dbApi.processos}/${args.id}`)
                .then(response => response.data);
        },
        partes() {
            return axios
                .get(config.dbApi.partes)
                .then(response => response.data);
        },

        parte(root, args) {
            return axios
                .get(`${config.dbApi.partes}/${args.id}`)
                .then(response => response.data);
        }
    },
    Processo: {
        partes(processo) {
            return axios
                .get(config.dbApi.processoPartes, {
                    params: {
                        processoId: processo.id
                    }
                })
                .then(response => {
                    const partes = response.data;
                    const promises = partes.map(processoParte => {
                        return axios
                            .get(`${config.dbApi.partes}/${processoParte.parteId}`)
                            .then(response => response.data);
                    });
                    return Promise.all(promises);
                });
        }
    },
    Parte: {
        processos(parte) {
            return axios
                .get(config.dbApi.processoPartes, {
                    params: {
                        parteId: parte.id
                    }
                })
                .then(response => {
                    const partes = response.data;
                    const promises = partes.map(processoParte => {
                        return axios
                            .get(`${config.dbApi.processos}/${processoParte.processoId}`)
                            .then(response => response.data);
                    });
                    return Promise.all(promises);
                });
        }
    },

    Mutation: {
        updateProcesso(root, {
            id,
            descricao
        }) {
            return axios
                .put(`${config.dbApi.processos}/${id}`, {
                    id,
                    descricao
                })
                .then(response => {
                    const processo = response.data;
                    pubsub.publish('processoUpdated', {
                        processoUpdated: processo
                    });
                    return processo;
                });
        },
    },

    Subscription: {
        processoUpdated: {
            subscribe: () => pubsub.asyncIterator('processoUpdated')
        },
    }
};
